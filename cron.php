<?php
include('common.php');

$sql = "SELECT * FROM hosts";
$result = sqlite_query($db,$sql);
while($row = sqlite_fetch_array($result, SQLITE_ASSOC))
{
    switch($row['type'])
    {
        case 1:
            $host = escapeshellarg($row['host']);
            $cmd = "fping $host";

            $exec = exec($cmd);

            if(substr_count($exec, 'is alive')==1)
            {
                $alive = true;
            } else
            {
                $alive = false;
            }
        break;
        case 2:
            $host = $row['host'];

            $result = file_get_contents($host);

            if($result)
            {
                $alive = true;
            } else
            {
                $alive = false;
            }
        break;
        case 3:
            $host = $row['host'];
            $substring = $row['substring'];

            $result = file_get_contents($host);

            if($result && substr_count($result, $substring)>0)
            {
                $alive = true;
            } else
            {
                $alive = false;
            }
        break;
    }

    if($alive && !$row['laststatus'])
    {
        $text = "Хост $host доступен";
        apiRequestJSON("sendMessage", array('chat_id' => $row['user_id'], "parse_mode" => 'Markdown', "text" => $text));
    }
    if(!$alive && $row['laststatus'])
    {
        $text = "Хост $host недоступен";
        apiRequestJSON("sendMessage", array('chat_id' => $row['user_id'], "parse_mode" => 'Markdown', "text" => $text));
    }
}