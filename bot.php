<?php
include('common.php');

/* config.php : 
define('BOT_TOKEN', '');
define('API_URL', 'https://api.telegram.org/bot'.BOT_TOKEN.'/');
define('WEBHOOK_URL', '');
define('BOT_USERNAME', '@');
*/

function processMessage($message) {
    global $db;
  // process incoming message
  $message_id = $message['message_id'];
  $chat_id = $message['chat']['id'];
  $userid = $message['from']['id'];
  $username = $message['from']['first_name'].' '.$message['from']['last_name'];
  
  if (isset($message['text']) && $chat_id > 0) {
    // incoming text message
    $text = $message['text'];
    $args = explode(' ', $text, 3);

    $cmd = $args[0];
    
    switch($cmd)
    {

        case '/start':
        case '/help':
            $text = "Бот будет отслеживать ваши сервера и уведомлять, если вдруг они станут недоступны.
Мы поддерживаем проверку по протоколу icmp (ping) и http. При проверке http вы можете указать слово либо фразу, которую мы будем искать на странице. Если данного слова не будет (например, вместо нормального сайта загрузилась страница с ошибкой) - сработает уведомлеие
Варианты использования:
/add myserver.com - добавить проверку по icmp
/add http://myserver.com - добавить проверку по http
/add http://myserver.com слово - добавить проверку по http с дополнительной проверкой наличия подстроки
/list - список проверок
/delete ID - удалить проверку по ID из списка";
            apiRequestJSON("sendMessage", array('chat_id' => $chat_id, "parse_mode" => 'Markdown', "text" => $text));
        break;
        case '/add':
            $host = mysql_escape_string($args[1]);
            
            if(substr_count($host, '://')>0)
            {
                // Проверка по http

                if(isset($args[2]))
                {
                    $substring = mysql_escape_string($args[2]);
                    $sql = "INSERT INTO hosts (host, substring, user_id, time, type, laststatus) VALUES
                    ('$host', '$substring', $chat_id, ".time().', 3, 0)';
                } else
                {
                    $sql = "INSERT INTO hosts (host, user_id, time, type, laststatus) VALUES
                    ('$host', $chat_id, ".time().', 2, 0';
                }
            } else
            {
                $sql = "INSERT INTO hosts (host, user_id, time, type, laststatus) VALUES
                    ('$host', $chat_id, ".time().', 1, 0)';
            }
            sqlite_query($db, $sql);

            $text = "Проверка успешно добавлена";
            apiRequestJSON("sendMessage", array('chat_id' => $chat_id, "parse_mode" => 'Markdown', "text" => $text));
        break;
        case '/list':
            $sql = "SELECT * FROM hosts WHERE user_id = $chat_id";
            $result = sqlite_query($db, $sql);
            $text = '';
            while($row = sqlite_fetch_array($result, SQLITE_ASSOC))
            {
                $text .= "ID: $row[host_id] Host: $row[host]\n";
            }
            apiRequestJSON("sendMessage", array('chat_id' => $chat_id, "parse_mode" => 'Markdown', "text" => $text));
        break;
        case '/delete':
            $id = intval($args[1]);

            $sql = "DELETE FROM hosts WHERE user_id = $chat_id AND host_id = $id";
            sqlite_query($db, $sql);
            $text = "Оповещение с ID=$id успешно удалено";
            apiRequestJSON("sendMessage", array('chat_id' => $chat_id, "parse_mode" => 'Markdown', "text" => $text));
        break;
        default:
            // = 'Неизвестная команда';
            apiRequestJSON("sendMessage", array('chat_id' => $chat_id, "parse_mode" => 'Markdown', "text" => 'Неизвестная команда'));
        break;
    }
  }
}


if (php_sapi_name() == 'cli') {
  // if run from console, set or delete webhook
  apiRequest('setWebhook', array('url' => isset($argv[1]) && $argv[1] == 'delete' ? '' : WEBHOOK_URL));
  exit;
}

$content = file_get_contents("php://input");
$update = json_decode($content, true);

if (!$update) {
  // receive wrong update, must not happen
  exit;
}

if (isset($update["message"])) {
  processMessage($update["message"]);
}
